/* eslint-env node */
module.exports = {
  root: true,
  'extends': [
    'plugin:vue/vue3-essential',
    'eslint:recommended'
  ],
  parserOptions: {
    ecmaVersion: 'latest'
  },
  rules: {
    // 取消 Vue3 组件强制命名规则：大驼峰 或 前-后
    // 'vue/multi-word-components-names': 0
  }
}
