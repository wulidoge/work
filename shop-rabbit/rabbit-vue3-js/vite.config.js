/** @format */

import { fileURLToPath, URL } from "node:url"
// import path from "node:path"

import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
// 配置按需导入 Element+ 先安装：npm install -D unplugin-vue-components unplugin-auto-import
import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import { ElementPlusResolver } from "unplugin-vue-components/resolvers"
// 配置手动导入
// import ElementPlus from "unplugin-element-plus/vite"
// import 'element-plus/es/components/message/style/css'
// import { ElMessage } from 'element-plus'
// import Inspect from "vite-plugin-inspect"

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        // 配置按需导入 Element+ 
        AutoImport({
            resolvers: [ElementPlusResolver()]
        }),
        Components({
            // 配置 Element+ 采用 Sass
            resolvers: [ElementPlusResolver({ importStyle: "sass" })]
        }),
        // 配置手动导入
        // ElementPlus({
        //     useSource: true,
        //     defaultLocale: "zh-tw"
        // }),
        // Inspect()
    ],
    resolve: {
        // 实现路径映射转换
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url))
            // 不用 element 官方文档的
            // "~/": `${path.resolve(__dirname, "src")}/`
        }
    },
    css: {
        // 预处理器 设置 npm i sass -D
        preprocessorOptions: {
            // 全局导入：设置 Vite 采用 Sass.scss
            scss: {
                // 自动导入定制主题的样式文件，进行覆盖
                // 自动导入 scss 主题色、变量
                additionalData: `
                @use "@/assets/styles/element/index.scss" as *;
                @use "@/assets/styles/var.scss" as *;`
            }
        }
    }
})
