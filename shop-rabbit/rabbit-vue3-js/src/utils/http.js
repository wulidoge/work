/**
 * axios API 基础封装
 *
 * @format
 */

import axios from "axios"
const httpInstance = axios.create({
    // 接口基地址
    baseURL: "http://pcapi-xiaotuxian-front-devtest.itheima.net",
    // 接口超时时间
    timeout: 5000
})
// 请求拦截器
httpInstance.interceptors.request.use(
    (config) => {
        return config
    },
    (e) => Promise.reject(e)
)
// 响应拦截器
httpInstance.interceptors.response.use(
    (res) => res.data,
    (e) => {
        return Promise.reject(e)
    }
)

export default httpInstance
