/** 配置 vue 路由
 * 1. 导入依赖
 * 2. 创建路由实例：配置历史模式、配置一级、二级路由
 * 3. 导出路由对象
 * @format
 */

/* 导入依赖 */
// 按需导入 vue-router 模块
import { createRouter, createWebHistory } from "vue-router";
// 导入一级路由：主页与登录页
import LayoutView from "@/views/Layout/LayoutView.vue";
import LoginView from "@/views/Login/LoginView.vue";
// 导入二级路由：首页默认页与首页分类页
import HomePage from "@/views/Home/HomePage.vue";
import CategoryPage from "@/views/Category/CategoryPage.vue";

/* 创建路由实例：router */
const router = createRouter({
  // 使用历史模式
  history: createWebHistory(import.meta.env.BASE_URL),
  // 配置路由组件
  routes: [
    // 配置主页路由
    {
      path: "/", // 用户访问地址
      component: LayoutView, // 对应组件
      // 配置二级路由，由于未发生页面跳转，都是页内操作，所以是二级
      children: [
        // 默认渲染页
        {
          path: "", // 通过路径置空会在主页渲染时一同渲染
          component: HomePage
        },
        // 分类页
        {
          path: "category",
          component: CategoryPage
        }
      ]
    },
    // 配置登录页路由，由于发生页面跳转，所以是一级路由
    {
      path: "/login",
      component: LoginView
    }
    // 文档示例
    // {
    //     path: "/",
    //     name: "home",
    //     component: HomeView
    // },
    // {
    //     path: "/about",
    //     name: "about",
    //     // route level code-splitting
    //     // this generates a separate chunk (About.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () => import("../views/AboutView.vue")
    // }
  ]
});

/* 导出路由对象 */
export default router;
