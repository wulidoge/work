/** @format */
/* 引入默认样式 */
// import 'element-plus/lib/theme-chalk/index.css'
// import "./assets/main.css"
import "@/assets/styles/main.css";

/* 引入 项目依赖 */
import { createApp } from "vue"
import { createPinia } from "pinia"
// 完整引入 element 
// # 按需导入在 vite.config.js 中配置
// import ElementPlus from "element-plus"
// import "element-plus/dist/index.css"

/* 导入 根组件与路由 */
import App from "./App.vue"
import router from "./router"

/* 创建 vue 实例 */
const app = createApp(App)
// 使用 Pinia VueRouter Element+
app.use(createPinia()).use(router)
// app.use(ElementPlus)
// 挂载 vue 实例
app.mount("#app")

// 测试接口函数
// 由于黑马小兔鲜后端 API 开启了跨域，所以能成功获取数据
// import { getCategoryAPI } from "./apis/testApi"
// getCategoryAPI().then((res) => {
//     console.log(res)
// })
