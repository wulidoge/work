# 黑马小兔鲜

## 项目简介

文件说明：

```python
# 代码风格：eslintrc.cjs
# 代码管理：gitignore
# 主页索引：index.html
# 本地配置：jsconfig.json
# 自动依赖：packge-lock.json
# 手动依赖：packge.json
# 项目文档：README.md
# 脚手架配置：vite.config.js
```

目录说明：

```python
# 接口函数：apis  
# 复用组件：components
# 组合函数：composables
# 全局指令：directives
# 路由配置：router
# 状态管理：stores
# 全局样式：style
# 测试文件：test
# 工具函数：utils
# 路由组件：views
# 根组件：App.vue
# 主函数：main.js
```

项目配置：

```python
# 状态管理 pinia 配置：创建项目时 yes
## 命令：
## 路径：

# 动态路由 vue-router 配置：创建项目时 yes
## 命令：
## 相关路径：@/router/index.js @/main.js

# element 配置：element-plus、按需导入插件
## 命令：npm i element-plus
## 命令：npm install -D unplugin-vue-components unplugin-auto-import
## 相关路径：./vite.config.js 按需导入，@/main.js 完整导入

# scss 配置：自定义 element 主题
## 命令：npm i sass -D
## 相关路径./vite.config.js @/styles/var.scss @/styles/element/index.scss

# axios 配置：
## npm i axios
## 相关路径：@\utils\http.js

```
