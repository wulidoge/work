// pages/goods/components/SearchBox/SearchBox.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    type:{
      type:String,
      // 导航搜索：navigator
      // 默认搜索：search-box
      // 按钮搜索：button
      value:"search-box"
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    
  }
})