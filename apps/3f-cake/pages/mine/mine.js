// pages/mine/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuOptions: [
      {
        name: "购物车",
        imgUrl: "/static/icons/icon_menuOptions/cart-fill.png",
        pageUrl: "/pages/cart/cart"
      },
      {
        name: "赠品",
        imgUrl: "/static/icons/icon_menuOptions/gifts.png",
        pageUrl: "/pages/mine/gifts/gifts"
      },
      {
        name: "收货地址",
        imgUrl: "/static/icons/icon_menuOptions/address.png",
        pageUrl: "/pages/mine/address/address"
      },
      {
        name: "个人信息",
        imgUrl: "/static/icons/icon_menuOptions/user-info.png",
        pageUrl: "/pages/mine/userInfo/userInfo"
      },
      {
        name: "账号与安全",
        imgUrl: "/static/icons/icon_menuOptions/privacy.png",
        pageUrl: "/pages/mine/privacy/privacy"
      }
    ]
  },
  // 获取菜单设置项列表
  getMenuOptions() {
    wx.request({
      url: 'url',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getMenuOptions()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})