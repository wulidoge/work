// pages/mine/components/MenuOption/MenuOption.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    name: {
      type: String,
      value: "菜单选项"
    },
    imgUrl: {
      type: String,
      value: "/static/icons/icon_menuOptions/gifts.png"
    },
    pageUrl: {
      type: String,
      value: "/pages/mine/gifts/gifts.js"
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleTap(e) {
      const pageUrl = this.data.pageUrl
      this.data.name === "购物车" ?
        wx.switchTab({
          url: pageUrl,
        }) : wx.navigateTo({
          url: pageUrl,
        })
    }
  }
})