// pages/mine/components/VipCard/VipCard.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    userInfo: { name: "六七", experience: 123, iconUrl: "/static/images/logo-circle.png" },
    cardInfoList: [
      { info: "", name: "余额", value: 0.00 },
      { info: "", name: "积分", value: 0 },
      { info: "", name: "卡", value: 0 },
      { info: "", name: "优惠劵/码", value: 0 }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})