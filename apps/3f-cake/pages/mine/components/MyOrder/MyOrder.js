// pages/mine/components/MyOrder/MyOrder.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },
  handleTap(e) {
    console.log(e)
  },
  /**
   * 组件的初始数据
   */
  data: {
    orderListUrl: "/pages/mine/orderList/orderList",
    myOrderList: [
      {
        name: "待付款",
        imgUrl: "/static/icons/icon_orderList/wait-for-payment.png",
        pageUrl: "/pages/mine/orderList/orderList"
      },
      {
        name: "待发货",
        imgUrl: "/static/icons/icon_orderList/wait-for-sending.png",
        pageUrl: "/pages/mine/orderList/orderList"
      },
      {
        name: "待收货",
        imgUrl: "/static/icons/icon_orderList/wait-for-delivery.png",
        pageUrl: "/pages/mine/orderList/orderList"
      },
      {
        name: "评价",
        imgUrl: "/static/icons/icon_orderList/comlist.png",
        pageUrl: "/pages/mine/orderList/orderList"
      },
      {
        name: "退款/售后",
        imgUrl: "/static/icons/icon_orderList/after-sale.png",
        pageUrl: "/pages/mine/orderList/orderList"
      }]
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})