// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList: ["/static/images/slide.jpg", "/static/images/slide.jpg", "/static/images/slide.jpg"],
    getSwiperListSucceed: false,
    cardList: ["/static/images/card.jpg", "/static/images/card.jpg", "/static/images/card.jpg"],
    getCardListSucceed: false
  },
  getSwiperList() {
    console.log("获取首页轮播图列表")
    // wx.request({
    //   url: 'url',
    // })
  },
  getCardList() {
    console.log("获取首页商品卡片列表")
    // wx.request({
    //   url: 'url',
    // })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getSwiperList()
    this.getCardList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})