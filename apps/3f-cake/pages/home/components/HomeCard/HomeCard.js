// components/HomeCard/HomeCard.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    goodsInfo: {
      type: Object,
      value: {
        id: "0001",
        goodsName: "初恋蛋糕",
        imgURL: "/static/images/card.jpg"
      }
    }

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    gotoGoodsPage() { }
  }
})