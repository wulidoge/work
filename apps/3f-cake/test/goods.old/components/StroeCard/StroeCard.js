// pages/goods/components/StroeCard/StroeCard.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    currentTab: 0,
    tabList: [
      {
        tabName: "商品选购",
        compName: "ShoppingBoard",
        tabClass: "nav-tab--active",
        boardClass: "page"
      },
      {
        tabName: "顾客评价",
        compName: "CommentsBoard",
        tabClass: "nav-tab",
        compClass: "page"
      },
      {
        tabName: "商家动态",
        compName: "NoticeBoard",
        tabClass: "nav-tab",
        compClass: "page"
      }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 页内导航切换
    switchTab(e) {
      let index = this.data.currentTab
      const tabList = this.data.tabList
      tabList[index].tabClass = "nav-tab"
      tabList[index].compClass = "page"
      index = e.currentTarget.dataset.index
      tabList[index].tabClass = "nav-tab--active"
      tabList[index].compClass = "page--active"
      this.setData({ currentTab: index, tabList })
    },
  }
})