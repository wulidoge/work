// custom-tab-bar/index.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    selected: 0,
    "list": [
      {
        "pagePath": "pages/home/home",
        "text": "首页",
        "iconPath": "../static/icons/icon_tabBar/store.png",
        "selectedIconPath": "../static/icons/icon_tabBar/store-active.png"
      },
      {
        "pagePath": "pages/goods/goods",
        "text": "商品",
        "iconPath": "../static/icons/icon_tabBar/goods.png",
        "selectedIconPath": "../static/icons/icon_tabBar/goods-active.png"
      },
      {
        "pagePath": "pages/cart/cart",
        "text": "购物车",
        "iconPath": "../static/icons/icon_tabBar/basket.png",
        "selectedIconPath": "../static/icons/icon_tabBar/basket-active.png"
      },
      {
        "pagePath": "pages/mine/mine",
        "text": "我的",
        "iconPath": "../static/icons/icon_tabBar/mine.png",
        "selectedIconPath": "../static/icons/icon_tabBar/mine-active.png"
      }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    handleTap(e) {
      let index = e.currentTarget.dataset.index
      let path = e.currentTarget.dataset.path
      this.setData({ selected: index })
      wx.switchTab({ url: path })
    }
  }
})