/** @format */

// 导入依赖
const express = require("express")
// const multer = require("multer")
const path = require("path")
const fs = require("fs")

// 创建服务器实例
const app = express()
const HOST = "127.0.0.1"
const PORT = process.env.PORT || 2567

// 使用内 static 中间件代理静态资源
app.use("/slides", express.static(path.join(__dirname, "public/images/swipers")))
// app.use("/static", express.static(path.join(__dirname, "public")))
// app.use(express.static("public"))

// 挂载路由
// app.get("/", (req, res) => {
//     res.send("Welcome")
// })
app.get("/slides", (req, res) => {
    const DIR = path.join(__dirname, "public/images/swipers")
    console.log(DIR)
    // 异步读取目录
    // fs.readdir(DIR, (err, files) => {
    //     if (err) {
    //         return res.status(500).send("Error loading images")
    //     }
    //     // console.log(files)
    //     res.send(files)
    // })
    // 同步读取目录
    const files = fs.readdirSync(DIR)
    const imagesUrl = files.map(item => `http://${HOST}:${PORT}/slides/${item}`)
    res.send(imagesUrl)
})

// 启动服务，监听端口
const server = app.listen(PORT, HOST, () => {
    console.log(`服务已启动，请访问：http://${HOST}:${PORT}`)
})
