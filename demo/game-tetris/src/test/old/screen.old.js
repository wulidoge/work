/**
 * 初始化屏幕
 *
 * @format
 */

/* 模拟液晶屏 */
;(() => {
    // 定义表格生成函数：返回 HTML 字符串 （尽量少用createElement()用模板字符串替代）
    function strMul(str, num) {
        return num > 1 ? (str += strMul(str, --num)) : str
    }
    function creatTable(row, col, cell) {
        let temp = ""
        temp = strMul(cell, col)
        const newRow = `<tr>${temp}</tr>`
        temp = ""
        temp = strMul(newRow, row)
        const table = `<table><tbody>${temp}</tbody></table>`
        return table
    }
    // 定义像素单元格
    const cell = '<td class="pixel"><div></div></td>'
    // 生成屏幕：16*24
    document.querySelector(".gameBoard").innerHTML = creatTable(24, 16, cell)
    // 生成计分板
    const Next = document.querySelector(".scoreBoard").children[2].children[1]
    Next.innerHTML = creatTable(4, 4, cell)
})()

/* 屏幕驱动 API */
// const screen = {
//     /* 主驱动 */
//     main: (selector, data) => {
//         const boardRow = document.querySelector(selector).children
//         if (typeof data === 'object') {
//             const pixels = []
//             for (let i = 0; i < boardRow.length; i++) {
//                 pixels.push(boardRow[i].children)
//             }
//             for (let i = 0; i < data.length; i++) {
//                 for (let j = 0; j < data[i].length; j++) {
//                     data[i][j]
//                         ? ([
//                               pixels[i][j].className,
//                               pixels[i][j].children[0].className
//                           ] = ["pixel Dark", "Dark"])
//                         : ([
//                               pixels[i][j].className,
//                               pixels[i][j].children[0].className
//                           ] = ["pixel", ""])
//                 }
//             }
//         } else {
//             return { width: boardRow.children.length, height: boardRow.length }
//         }
//     },
//     /* 主面板 */
//     game: (data) => {
//         screen.main(".gameBoard tbody", data)
//     },
//     gameSize: { width: screen.main(".gameBoard tbody",0).width, height: screen.main(".gameBoard tbody").height },
//     /* 计分板：Score */
//     score: () => {},
//     /* 计分板：Level */
//     level: () => {},
//     /* 计分板：Next */
//     next: (data) => {
//         screen.main(".scoreBoard .next>div:last-child tbody", data)
//     },
//     nextSize: { width: screen.main(".scoreBoard .next>div:last-child tbody").width, height: screen.main(".scoreBoard .next>div:last-child tbody",0).height },
//     /* 计分板：Time */
//     time: () => {}
// }

// screen.game([1,1])

// export default { screen }
