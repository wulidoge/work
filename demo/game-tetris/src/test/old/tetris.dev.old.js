/** 《俄罗斯方块·方块创建模块》
 * 参数：无
 * 功能：调用后在屏幕顶部随机位置获得一个匀速下落的随机类型的俄罗斯方块
 * 返回值：该方块的控制器，可控制方块左右移动、快输下坠、旋转
 * I L O T Z l z
 * @format */

;(function () {
    function getTetris(data, drive) {
        const types = "ILlOTZz"
        const tetris = {
            state: 0,
            pixels: [{ x: 3, y: 3 }],
            initTeris: function () {
                /* 初始化方块 */
            },
            setPixels: function () {},
            setData: function (lighten) {
                this.pixels.forEach((pixel) => {
                    data[pixel.y][pixel.x] = lighten
                })
            },
            moveLeft: function () {
                /* 左移方块 */
            },
            moveRight: function () {
                /* 右移方块 */
            },
            moveDown: function () {
                /* 下移方块 */
            },
            rotate: function () {
                /* 旋转方块 */
            },
            next: function (data, pixels, direction) {
                /* 返回能否移动 */
            }
        }
        const randomIndex = (num) => Math.floor(Math.random() * num)
        const initPixels = (num) =>
            new Array(num).fill("0").map(() => new Object())
        // 获取随机类型方块 types[randomIndex(7)]

        switch ("O") {
            case "I":
                tetris.state = randomIndex(2)
                if (tetris.state === 0) {
                    // 状态0：竖着，取最下方像素为主像素
                    tetris.setPixels = function () {
                        this.pixels[1].x = this.pixels[0].x
                        this.pixels[1].y = this.pixels[0].y - 1
                        this.pixels[2].x = this.pixels[0].x
                        this.pixels[2].y = this.pixels[0].y - 2
                        this.pixels[3].x = this.pixels[0].x
                        this.pixels[3].y = this.pixels[0].y - 3
                    }
                    tetris.moveLeft = function () {}
                    tetris.moveRight = function () {}
                    tetris.moveDown = function () {}
                    tetris.rotate = function () {}
                } else {
                    // 状态1：横着，取最左方像素为主像素
                    tetris.setPixels = function () {
                        this.pixels[1].x = this.pixels[0].x + 1
                        this.pixels[1].y = this.pixels[0].y
                        this.pixels[2].x = this.pixels[0].x + 2
                        this.pixels[2].y = this.pixels[0].y
                        this.pixels[3].x = this.pixels[0].x + 3
                        this.pixels[3].y = this.pixels[0].y
                    }
                    tetris.moveLeft = function () {}
                    tetris.moveRight = function () {}
                    tetris.moveDown = function () {}
                    tetris.rotate = function () {}
                }
                break
            case "L":
                break
            case "l":
                break
            case "O":
                // 根据主像素位置计算其余像素位置
                tetris.setPixels = function () {
                    this.pixels[1].x = this.pixels[0].x
                    this.pixels[1].y = this.pixels[0].y - 1
                    this.pixels[2].x = this.pixels[0].x + 1
                    this.pixels[2].y = this.pixels[0].y - 1
                    this.pixels[3].x = this.pixels[0].x + 1
                    this.pixels[3].y = this.pixels[0].y
                }
                // 初始化方块
                //tetris.initTeris()
                tetris.pixels = initPixels(4)
                tetris.pixels[0].x = randomIndex(data.length - 1)
                tetris.pixels[0].y = 3
                tetris.setPixels()
                tetris.setData(1)
                drive(data)
                //
                tetris.moveLeft = function () {
                    const pixel0 = data[this.pixels[0].y][this.pixels[0].x - 1]
                    const pixel1 = data[this.pixels[1].y][this.pixels[1].x - 1]
                    // 判断能否移动
                    if (pixel0 + pixel1 === 0) {
                        // 熄灭原像素
                        this.setData(0)
                        // 主像素左移
                        this.pixels[0].x--
                        // 其余像素左移
                        this.setPixels()
                        // 点亮新像素
                        this.setData(1)
                    }
                }
                //
                tetris.moveRight = function () {
                    const pixel2 = data[this.pixels[2].y][this.pixels[2].x + 1]
                    const pixel3 = data[this.pixels[3].y][this.pixels[3].x + 1]
                    if (pixel2 + pixel3 === 0) {
                        this.setData(0)
                        this.pixels[0].x++
                        this.setPixels()
                        this.setData(1)
                    }
                }
                //
                tetris.moveDown = function () {
                    const pixelY = data[this.pixels[0].y + 1]
                    // 判断数组索引是否越界，避免 undefined 报错
                    if (pixelY) {
                        const pixel0 =
                            data[this.pixels[0].y + 1][this.pixels[0].x]
                        const pixel3 =
                            data[this.pixels[3].y + 1][this.pixels[3].x]
                        if (pixel0 + pixel3 === 0) {
                            this.setData(0)
                            this.pixels[0].y++
                            this.setPixels()
                            this.setData(1)
                        }
                    }
                }
                // 方块 O：为正方形，不用定义旋转函数
                break
            case "T":
                break
            case "Z":
                break
            case "z":
                break
            // default:
        }

        function handle() {
            const controller = (event) => {
                switch (event.key) {
                    // case "ArrowUp":
                    //     this.moveUp()
                    //     break
                    case "ArrowDown":
                        tetris.moveDown()
                        drive(data)
                        break
                    case "ArrowLeft":
                        tetris.moveLeft()
                        drive(data)
                        break
                    case "ArrowRight":
                        tetris.moveRight()
                        drive(data)
                        break
                }
                console.log(data, tetris)
            }
            window.removeEventListener("keydown", controller)
            window.addEventListener("keydown", controller)
        }
        return handle
    }
    window.getTetris = getTetris
})()
