/** @format */

;(function () {
    function createPixels(col = 4, row = 4, dom) {
        const pixels = document.createElement("table")
        const memory = []
        for (let i = 0; i < col * row; i++) {
            if (i % col === 0) {
                memory.push([])
                pixels.appendChild(document.createElement("tr"))
                addPixel()
            } else {
                addPixel()
            }
        }
        function addPixel() {
            memory[memory.length - 1].push(document.createElement("td"))
            pixels.children[memory.length - 1].appendChild(
                memory[memory.length - 1][memory[memory.length - 1].length - 1]
            )
        }
        pixels.className = "pixels"
        dom.removeChild(dom.children[0])
        dom.appendChild(pixels)
        function drive(data) {
            const index = { x: 0, y: 0 }
            for (let i = 0; i < row * col; i++) {
                ;[index.x, index.y] = [i % col, Math.floor(i / col)]
                data[index.y] && data[index.y][index.x]
                    ? (memory[index.y][index.x].className = "light")
                    : (memory[index.y][index.x].className = "dark")
            }
        }
        return drive
    }
    window.createPixels = createPixels
})()
