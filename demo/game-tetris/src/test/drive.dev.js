/** 《俄罗斯方块·像素构造模块》
 * @参数 num:col=4 num:row=4 domObj:dom
 * @功能 构造并挂载 col*row 大小的虚拟像素表到虚拟屏幕的指定面板 dom 上
 * @返回值 像素表驱动函数 drive(data) 传入二维数组，刷新像素表显示
 * 用法示例：
 * # <script src='./src/js/createPixels.js'></script>
 * # <script>
 * #   const drive = createPixels(6, 8, document.querySelector('#screen-main'))
 * # </script>
 * #
 * # 未支持 import CreatePixel from './src/js/CreatePixel.js'
 * # 未支持 const screenMain = new CreatePixel(20,10,screenMain)
 * @format */

// 立即执行函数包裹，避免全局变量污染
;(function () {
    function getDrive(dom, row = 12, col = 12) {
        const pixels = document.createElement("table")
        const memory = []

        // 构造虚拟像素表格
        for (let i = 0; i < row * col; i++) {
            if (i % col === 0) {
                // 新起一行
                memory.push([])
                pixels.appendChild(document.createElement("tr"))
                addPixel(i)
            } else {
                addPixel(i)
            }
        }

        // 定位到当前行，并添加像素
        function addPixel(i) {
            // const indexR = memory.length - 1
            const indexR = Math.floor(i / col)
            memory[indexR].push(document.createElement("td"))
            // const indexC = memory[indexR].length - 1
            const indexC = i % col
            pixels.children[indexR].appendChild(memory[indexR][indexC])
        }

        // 为虚拟像素表添加样式，并挂载到指定 DOM 如 document.querySelector('#screen-main')
        pixels.className = "pixels"
        dom.removeChild(dom.children[0])
        dom.appendChild(pixels)

        let count = 0
        // 定义驱动函数
        function drive(data) {
            count++
            console.log("from drive.dev.js 当前帧数：", count, data)
            console.log("from drive.dev.js 显存状态：",  memory)
            const index = { x: 0, y: 0 }
            for (let i = 0; i < row * col; i++) {
                ;[index.x, index.y] = [i % col, Math.floor(i / col)]
                data[index.y] && data[index.y][index.x]
                    ? (memory[index.y][index.x].className = "light")
                    : (memory[index.y][index.x].className = "dark")
            }
        }
        return drive
    }

    // 将模块挂载到全局对象
    window.getDrive = getDrive
})()
